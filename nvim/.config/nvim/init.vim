call plug#begin('~/.local/share/nvim/plugged')
    Plug 'SirVer/ultisnips'
    Plug 'morhetz/gruvbox'
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    Plug 'honza/vim-snippets'
    Plug 'tpope/vim-sensible'
    Plug 'tpope/vim-fugitive'
    Plug 'scrooloose/nerdtree'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-syntastic/syntastic'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'Rip-Rip/clang_complete'
    Plug 'Shougo/neco-vim'
    Plug 'tpope/vim-surround'
    Plug 'majutsushi/tagbar'
    Plug 'lervag/vimtex'
    Plug 'terryma/vim-multiple-cursors'
    Plug 'dylanaraps/wal.vim'
    Plug 'thaerkh/vim-workspace'
    Plug 'chriskempson/base16-vim'
call plug#end()

set tabstop=8
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab
set number
set mouse=a
set modifiable

set t_8f=^[[38;2;%lu;%lu;%lum        " set foreground color
set t_8b=^[[48;2;%lu;%lu;%lum        " set background color

set t_Co=256                         " Enable 256 colors
set termguicolors                    " Enable GUI colors for the terminal to get truecolor

colorscheme base16-tomorrow-night

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

set listchars=tab:>·,trail:~,extends:>,precedes:<

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:deoplete#enable_at_startup=1
let g:airline_gruvbox_bg='dark'
let g:airline_powerline_fonts = 1

let g:vimtex_view_method = 'zathura'

let g:deoplete#enable_at_startup=1

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
"let g:UltiSnipsEditSplit="vertical"

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

map <C-f> :NERDTreeToggle<CR>

map <Leader>[ :nohl<CR>
map <Leader>] :w<CR>
map <Leader>= :SyntasticToggleMode<CR>
map <C-d> :set spell!<CR>

set spelllang=ru,en

"autocmd TextChanged,TextChangedI <buffer> silent write
let g:workspace_autosave_always = 1
set updatetime=5000

let g:workspace_autosave_untrailspaces = 0

set spell spelllang=en_us
